//go:generate go run generator.go

/*
© 2017 Paul John King (paul_john_king@web.de).  All rights reserved.

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License, version 3 as published by the Free
Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package revet

import (
	. "path"

	"bytes"
	"fmt"
	"os"
	"os/exec"
	"regexp"
)

/*
'gitDescription' tries to return a Git description 'description' of the 'HEAD'
commit to a Git repository.  If the working directory of the repository
contains the file-system path 'path' then 'description' is

	«tag»-«count»-g«id»

or

	«tag»-«count»-g«id»-dirty

where

-	«tag» is the most recent annotated tag of the closest previous commit with
an annotated tag,

-	«count» is the number of commits since the closest previous commit with an
annotated tag,

-	«id» is the full ID of the 'HEAD' commit, and

-	'description' has suffix '-dirty' if, and only if, the repository is dirty
(as described in the 'Overview' section of the package documentation).

*/
func gitDescription(path string) (description []byte, err error) {
	var (
		command *exec.Cmd
		stat    os.FileInfo
	)

	command = exec.Command(`git`, `describe`, `--long`, `--abbrev=40`, `--dirty`)
	stat, err = os.Stat(path)
	if err != nil {
		return
	}
	if stat.IsDir() {
		command.Dir = path
	} else {
		command.Dir = Dir(path)
	}
	description, err = command.Output()
	if err != nil {
		switch err.(type) {
		case *exec.ExitError:
			err = fmt.Errorf("%s", bytes.TrimRight(err.(*exec.ExitError).Stderr, "\n\r"))
		}
		return
	}
	description = bytes.TrimRight(description, "\n\r")
	return
}

/*
'semanticVersion' tries to return a semantic versioning 2.0.0 compliant
semantic version 'version', a full Git commit ID 'id' and a Boolean flag
'dirty' extracted from the string 'description'.  If 'description' is

	«major».«minor»-«patch»-g«hex40»

or

	«major».«minor»-«patch»-g«hex40»-dirty

where

-	«major» is a semantic versioning 2.0.0 compliant major version,

-	«minor» is a semantic versioning 2.0.0 compliant minor version,

-	«patch» is a semantic versioning 2.0.0 compliant patch version, and

-	«hex40» is a string of 40 lower-case hexadecimal characters

then

-	'version' is «major».«minor».«patch»,

-	'id' is «hex40», and

-	'dirty' is true if, and only if, 'description' has suffix '-dirty'.

*/
func semanticVersion(description []byte) (version string, id string, dirty bool, err error) {
	const (
		majorRegexp      string = `(?P<major>0|[123456789][0123456789]*)`
		minorRegexp      string = `(?P<minor>0|[123456789][0123456789]*)`
		preReleaseRegexp string = `(?P<preRelease>[[:alnum:]]+(-[[:alnum:]]+)*)`
		patchRegexp      string = `(?P<patch>0|[123456789][0123456789]*)`
		idRegexp         string = `(?P<id>[01234567890abcdef]{40})`
		dirtyRegexp      string = `(?P<dirty>dirty)`
	)

	var (
		descriptionRegexp *regexp.Regexp
	)

	descriptionRegexp = regexp.MustCompile(
		`^` +
			majorRegexp + `\.` + minorRegexp +
			`(-` + preReleaseRegexp + `)?` +
			`-` + patchRegexp +
			`-g` + idRegexp +
			`(-` + dirtyRegexp + `)?` +
			`$`,
	)
	if descriptionRegexp.Match(description) {
		var (
			submatches       [][]byte
			namedSubmatchMap map[string][]byte
			major            string
			minor            string
			preRelease       string
			patch            string
			buffer           bytes.Buffer
		)

		submatches = descriptionRegexp.FindSubmatch(description)
		namedSubmatchMap = make(map[string][]byte)
		for index, name := range descriptionRegexp.SubexpNames() {
			if index != 0 {
				namedSubmatchMap[name] = submatches[index]
			}
		}

		major = string(namedSubmatchMap[`major`])
		minor = string(namedSubmatchMap[`minor`])
		preRelease = string(namedSubmatchMap[`preRelease`])
		patch = string(namedSubmatchMap[`patch`])
		id = string(namedSubmatchMap[`id`])
		if len(namedSubmatchMap[`dirty`]) != 0 {
			dirty = true
		}

		buffer.WriteString(major)
		buffer.WriteString(`.`)
		buffer.WriteString(minor)
		buffer.WriteString(`.`)
		if len(preRelease) != 0 {
			buffer.WriteString(`0-`)
			buffer.WriteString(preRelease)
			buffer.WriteString(`.`)
		}
		buffer.WriteString(patch)

		version = buffer.String()
		return
	} else {
		err = fmt.Errorf("fatal: Cannot extract valid semantic version from %q", description)
	}
	return
}

/*
'SemanticVersion' tries to return the semantic versioning 2.0.0 compliant
semantic version 'version' (as described in the 'Overview' section of the
package documentation) and the full commit ID 'id' of the 'HEAD' commit to the
Git repository whose working directory contains the file-system path 'path', as
well as a Boolean flag 'dirty' that is true if, and only if, the repository is
dirty (as described in the 'Overview' section of the package documentation).

*/
func SemanticVersion(path string) (version string, id string, dirty bool, err error) {
	var (
		description []byte
	)

	description, err = gitDescription(path)
	if err != nil {
		return
	}
	version, id, dirty, err = semanticVersion(description)
	if err != nil {
		return
	}
	return
}
