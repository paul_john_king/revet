//usr/bin/env go run "${0}" "${@}"; exit $?

/*
© 2017 Paul John King (paul_john_king@web.de).  All rights reserved.

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License, version 3 as published by the Free
Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package main

import (
	. "gitlab.com/paul_john_king/revet"

	"flag"
	"fmt"
	"io"
	"os"
)

func main() {
	var (
		getMessageWriter = func(writer io.Writer, message string) (writeMessage func()) {
			writeMessage = func() {
				fmt.Fprintf(writer, message)
				return
			}
			return
		}
		writeShortHelpMessageToStderr = getMessageWriter(os.Stderr, ShortHelpMessage)
		writeShortHelpMessageToStdout = getMessageWriter(os.Stdout, ShortHelpMessage)
		writeLongHelpMessageToStdout  = getMessageWriter(os.Stdout, LongHelpMessage)

		err           error
		shortHelpFlag bool
		longHelpFlag  bool
		idFlag        bool
		path          string
		version       string
		id            string
		dirty         bool
	)

	flag.Usage = writeShortHelpMessageToStderr
	flag.BoolVar(&shortHelpFlag, `help`, false, ``)
	flag.BoolVar(&shortHelpFlag, `h`, false, ``)
	flag.BoolVar(&longHelpFlag, `Help`, false, ``)
	flag.BoolVar(&longHelpFlag, `H`, false, ``)
	flag.BoolVar(&idFlag, `id`, false, ``)
	flag.BoolVar(&idFlag, `i`, false, ``)
	flag.Parse()

	if shortHelpFlag {
		writeShortHelpMessageToStdout()
		os.Exit(0)
	}
	if longHelpFlag {
		writeLongHelpMessageToStdout()
		os.Exit(0)
	}
	switch flag.NArg() {
	case 0:
		path, err = os.Getwd()
		if err != nil {
			fmt.Fprintln(os.Stderr, err.Error())
			writeShortHelpMessageToStderr()
			os.Exit(2)
		}
	case 1:
		path = flag.Arg(0)
	default:
		fmt.Fprintln(os.Stderr, `too many command-line arguments`)
		writeShortHelpMessageToStderr()
		os.Exit(2)
	}

	version, id, dirty, err = SemanticVersion(path)
	if err != nil {
		fmt.Fprintln(os.Stderr, err.Error())
		writeShortHelpMessageToStderr()
		os.Exit(1)
	}
	switch {
	case !idFlag && !dirty:
		fmt.Println(version)
	case !idFlag && dirty:
		fmt.Println(version + `+dirty`)
	case idFlag && !dirty:
		fmt.Println(version + `+` + id)
	case idFlag && dirty:
		fmt.Println(version + `+` + id + `-dirty`)
	}
	return
}
