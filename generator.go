//+build ignore

/*
© 2017 Paul John King (paul_john_king@web.de).  All rights reserved.

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License, version 3 as published by the Free
Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
A documentation generator for the 'revet' package and command.  Call

	go run generator.go

in the root directory of the project to generate

	doc.go
	README.md
	revet/doc.go
	revet/long_help_message.go
	revet/short_help_message.go

from

	documentation.md

before building or installing the 'revet' package or command.

*/
package main

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"os"
	"regexp"
	"strings"
)

type fragments struct {
	_map      map[string]string
	copyright string
	doNotEdit string
}

func (frags *fragments) init(path string) (err error) {
	var (
		buffers       map[string]*bytes.Buffer
		beginRegex    *regexp.Regexp
		endRegex      *regexp.Regexp
		file          *os.File
		reader        *bufio.Reader
		str           string
		beginMatches  []string
		endMatches    []string
		writeContent  bool
		contentTarget string
		id            string
		frag          *bytes.Buffer
	)

	frags._map = make(map[string]string)
	buffers = make(map[string]*bytes.Buffer)
	beginRegex = regexp.MustCompile("^//begin:(.*)\n")
	endRegex = regexp.MustCompile("^//end:.*\n")
	file, err = os.Open(path)
	defer file.Close()
	if err != nil {
		return
	}
	reader = bufio.NewReader(file)
	for {
		str, err = reader.ReadString('\n')
		if err == io.EOF {
			err = nil
			break
		}
		if err != nil {
			return
		}
		beginMatches = beginRegex.FindStringSubmatch(str)
		endMatches = endRegex.FindStringSubmatch(str)
		switch {
		case len(beginMatches) > 1:
			writeContent = true
			contentTarget = beginRegex.FindStringSubmatch(str)[1]
			if buffers[contentTarget] == nil {
				buffers[contentTarget] = new(bytes.Buffer)
			}
			continue
		case len(endMatches) > 0:
			writeContent = false
			continue
		case writeContent:
			_, err = buffers[contentTarget].WriteString(str)
			if err != nil {
				return
			}
		}
	}
	for id, frag = range buffers {
		frags._map[id] = frag.String()
	}
	frags.copyright = frags.get(`copyright`)
	frags.doNotEdit = frags.get(`doNotEdit`)
	return
}

func (frags fragments) get(id string) (fragment string) {
	fragment = frags._map[id]
	return
}

func (frags fragments) writeMarkdownFile(path string, ids ...string) (err error) {
	var (
		strs []string
		id   string
	)

	for _, id = range ids {
		strs = append(strs, frags.get(id))
	}
	err = writeFile(
		path,
		xmlComment(frags.copyright), "\n",
		xmlComment(frags.doNotEdit), "\n",
		strings.Join(strs, "\n"),
	)
	if err != nil {
		return
	}
	return
}

func (frags fragments) writeDocGoFile(path string, pckg string, summary string, ids ...string) (err error) {
	var (
		strs []string
		id   string
	)

	for _, id = range ids {
		strs = append(strs, frags.get(id))
	}
	err = writeFile(
		path,
		goComment(frags.copyright), "\n",
		goComment(frags.doNotEdit), "\n",
		goComment(
			frags.get(summary), "\n",
			indent4(escapeBackticks(strings.Join(strs, "\n"))), "\n",
		),
		"package ", pckg, "\n",
	)
	if err != nil {
		return
	}
	return
}

func (frags fragments) writeVariableGoFile(path string, pckg string, name string, ids ...string) (err error) {
	var (
		strs []string
		id   string
	)

	for _, id = range ids {
		strs = append(strs, frags.get(id))
	}
	err = writeFile(
		path,
		goComment(frags.copyright), "\n",
		goComment(frags.doNotEdit), "\n",
		"package ", pckg, "\n\n",
		"var ", name, " string = `",
		escapeBackticks(strings.Join(strs, "\n")),
		"`\n",
	)
	if err != nil {
		return
	}
	return
}

func main() {
	var (
		frags fragments
		err   error
	)

	err = frags.init(`documentation.md`)
	if err != nil {
		checkError(err, 1)
	}
	err = frags.writeMarkdownFile(
		`README.md`,
		`readmeHeader`,
		`background`,
		`readmeFooter`,
	)
	if err != nil {
		checkError(err, 1)
	}
	err = frags.writeDocGoFile(
		`doc.go`,
		`revet`,
		`packageSummary`,
		`packageHeader`,
		`background`,
	)
	if err != nil {
		checkError(err, 1)
	}
	err = frags.writeDocGoFile(
		`revet/doc.go`,
		`main`,
		`commandSummary`,
		`commandHeader`,
		`background`,
	)
	if err != nil {
		checkError(err, 1)
	}
	err = frags.writeVariableGoFile(
		`revet/short_help_message.go`,
		`main`,
		`ShortHelpMessage`,
		`shortHelpMessage`,
	)
	if err != nil {
		checkError(err, 1)
	}
	err = frags.writeVariableGoFile(
		`revet/long_help_message.go`,
		`main`,
		`LongHelpMessage`,
		`shortHelpMessage`,
		`background`,
		`longHelpMessageFoot`,
	)
	if err != nil {
		checkError(err, 1)
	}
	return
}

func checkError(err error, status int) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error: %s\n", err.Error())
		os.Exit(status)
	}
	if err != nil {
		checkError(err, 1)
	}
	return
}

func writeFile(path string, strings ...string) (err error) {
	var (
		file *os.File
		str  string
	)

	file, err = os.Create(path)
	defer file.Close()
	if err != nil {
		return
	}
	for _, str = range strings {
		file.WriteString(str)
	}
	return
}

func goComment(ins ...string) (out string) {
	var (
		buffer bytes.Buffer
		str    string
	)
	buffer.WriteString("/*\n")
	for _, str = range ins {
		buffer.WriteString(str)
	}
	buffer.WriteString("*/\n")
	out = buffer.String()
	return
}

func xmlComment(ins ...string) (out string) {
	var (
		buffer bytes.Buffer
		str    string
	)
	buffer.WriteString("<!--\n")
	for _, str = range ins {
		buffer.WriteString(str)
	}
	buffer.WriteString("-->\n")
	out = buffer.String()
	return
}

func indent4(in string) (out string) {
	var (
		buffer bytes.Buffer
		str    string
	)

	for _, str = range strings.SplitAfter(in, "\n") {
		switch len(str) {
		case 0:
		case 1:
			buffer.WriteString(str)
		default:
			buffer.WriteString(`    `)
			buffer.WriteString(str)
		}
	}
	out = buffer.String()
	return
}

func escapeBackticks(in string) (out string) {
	out = strings.Replace(in, "`", "'", -1)
	return
}
