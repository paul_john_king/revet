/*
© 2017 Paul John King (paul_john_king@web.de).  All rights reserved.

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License, version 3 as published by the Free
Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package revet

import (
	"strings"
	"testing"
)

var (
	showError = map[bool]string{
		true:  `nil`,
		false: `not-nil`,
	}
)

func TestSemanticVersion(t *testing.T) {
	type (
		Datum struct {
			description string
			version     string
			commit      string
			dirty       bool
			nilError    bool
		}
		Data []Datum
	)

	const (
		functionName = `semanticVersion`
	)

	var (
		datum Datum
		data  Data
	)

	data = Data{

		//	Valid.
		{
			description: `0.0-0-g01234567890abcdef0123456789abcdef0123456`,
			version:     `0.0.0`,
			commit:      `01234567890abcdef0123456789abcdef0123456`,
			dirty:       false,
			nilError:    true,
		},
		{
			description: `0.0-0-g01234567890abcdef0123456789abcdef0123456-dirty`,
			version:     `0.0.0`,
			commit:      `01234567890abcdef0123456789abcdef0123456`,
			dirty:       true,
			nilError:    true,
		},
		{
			description: `1.2-3-g01234567890abcdef0123456789abcdef0123456`,
			version:     `1.2.3`,
			commit:      `01234567890abcdef0123456789abcdef0123456`,
			dirty:       false,
			nilError:    true,
		},
		{
			description: `1.2-3-g01234567890abcdef0123456789abcdef0123456-dirty`,
			version:     `1.2.3`,
			commit:      `01234567890abcdef0123456789abcdef0123456`,
			dirty:       true,
			nilError:    true,
		},
		{
			description: `111.222-333-g01234567890abcdef0123456789abcdef0123456`,
			version:     `111.222.333`,
			commit:      `01234567890abcdef0123456789abcdef0123456`,
			dirty:       false,
			nilError:    true,
		},
		{
			description: `111.222-333-g01234567890abcdef0123456789abcdef0123456-dirty`,
			version:     `111.222.333`,
			commit:      `01234567890abcdef0123456789abcdef0123456`,
			dirty:       true,
			nilError:    true,
		},

		//	Invalid major.
		{
			description: `2-3-g01234567890abcdef0123456789abcdef0123456`,
			version:     ``,
			commit:      ``,
			dirty:       false,
			nilError:    false,
		},
		{
			description: `.2-3-g01234567890abcdef0123456789abcdef0123456`,
			version:     ``,
			commit:      ``,
			dirty:       false,
			nilError:    false,
		},
		{
			description: `1$2-3-g01234567890abcdef0123456789abcdef0123456`,
			version:     ``,
			commit:      ``,
			dirty:       false,
			nilError:    false,
		},
		{
			description: `01.2-3-g01234567890abcdef0123456789abcdef0123456`,
			version:     ``,
			commit:      ``,
			dirty:       false,
			nilError:    false,
		},
		{
			description: `a.2-3-g01234567890abcdef0123456789abcdef0123456`,
			version:     ``,
			commit:      ``,
			dirty:       false,
			nilError:    false,
		},

		//	Invalid minor.
		{
			description: `1.3-g01234567890abcdef0123456789abcdef0123456`,
			version:     ``,
			commit:      ``,
			dirty:       false,
			nilError:    false,
		},
		{
			description: `1.-3-g01234567890abcdef0123456789abcdef0123456`,
			version:     ``,
			commit:      ``,
			dirty:       false,
			nilError:    false,
		},
		{
			description: `1.2$3-g01234567890abcdef0123456789abcdef0123456`,
			version:     ``,
			commit:      ``,
			dirty:       false,
			nilError:    false,
		},
		{
			description: `1.02-3-g01234567890abcdef0123456789abcdef0123456`,
			version:     ``,
			commit:      ``,
			dirty:       false,
			nilError:    false,
		},
		{
			description: `1.b-3-g01234567890abcdef0123456789abcdef0123456`,
			version:     ``,
			commit:      ``,
			dirty:       false,
			nilError:    false,
		},

		//	Invalid count.
		{
			description: `1.2-g01234567890abcdef0123456789abcdef0123456`,
			version:     ``,
			commit:      ``,
			dirty:       false,
			nilError:    false,
		},
		{
			description: `1.2--g01234567890abcdef0123456789abcdef0123456`,
			version:     ``,
			commit:      ``,
			dirty:       false,
			nilError:    false,
		},
		{
			description: `1.2-3$g01234567890abcdef0123456789abcdef0123456`,
			version:     ``,
			commit:      ``,
			dirty:       false,
			nilError:    false,
		},
		{
			description: `1.2-03-g01234567890abcdef0123456789abcdef0123456`,
			version:     ``,
			commit:      ``,
			dirty:       false,
			nilError:    false,
		},
		{
			description: `1.2-c-g01234567890abcdef0123456789abcdef0123456`,
			version:     ``,
			commit:      ``,
			dirty:       false,
			nilError:    false,
		},

		//	Invalid commit.
		{
			description: `1.2-3-`,
			version:     ``,
			commit:      ``,
			dirty:       false,
			nilError:    false,
		},
		{
			description: `1.2-3-g`,
			version:     ``,
			commit:      ``,
			dirty:       false,
			nilError:    false,
		},
		{
			description: `1.2-3-g0123456`,
			version:     ``,
			commit:      ``,
			dirty:       false,
			nilError:    false,
		},
		{
			description: `1.2-3-g01234567890abcdef0123456789abcdef012345`,
			version:     ``,
			commit:      ``,
			dirty:       false,
			nilError:    false,
		},
		{
			description: `1.2-3-g01234567890abcdef0123456789abcdef01234567`,
			version:     ``,
			commit:      ``,
			dirty:       false,
			nilError:    false,
		},
		{
			description: `1.2-3-G01234567890abcdef0123456789abcdef0123456`,
			version:     ``,
			commit:      ``,
			dirty:       false,
			nilError:    false,
		},
		{
			description: `1.2-3-x01234567890abcdef0123456789abcdef0123456`,
			version:     ``,
			commit:      ``,
			dirty:       false,
			nilError:    false,
		},
		{
			description: `1.2-3-g01234567890ABCDEF0123456789ABCDEF0123456`,
			version:     ``,
			commit:      ``,
			dirty:       false,
			nilError:    false,
		},
		{
			description: `1.2-3-g01234567890ghijkl0123456789ghijkl0123456`,
			version:     ``,
			commit:      ``,
			dirty:       false,
			nilError:    false,
		},

		//	Invalid 'dirty'.
		{
			description: `1.2-3-g01234567890abcdef0123456789abcdef0123456-`,
			version:     ``,
			commit:      ``,
			dirty:       false,
			nilError:    false,
		},
		{
			description: `1.2-3-g01234567890abcdef0123456789abcdef0123456-d`,
			version:     ``,
			commit:      ``,
			dirty:       false,
			nilError:    false,
		},
		{
			description: `1.2-3-g01234567890abcdef0123456789abcdef0123456-dirt`,
			version:     ``,
			commit:      ``,
			dirty:       false,
			nilError:    false,
		},
		{
			description: `1.2-3-g01234567890abcdef0123456789abcdef0123456-dirtyx`,
			version:     ``,
			commit:      ``,
			dirty:       false,
			nilError:    false,
		},
		{
			description: `1.2-3-g01234567890abcdef0123456789abcdef0123456-DIRTY`,
			version:     ``,
			commit:      ``,
			dirty:       false,
			nilError:    false,
		},
		{
			description: `1.2-3-g01234567890abcdef0123456789abcdef0123456-clean`,
			version:     ``,
			commit:      ``,
			dirty:       false,
			nilError:    false,
		},
	}

	for _, datum = range data {
		var (
			version  string
			commit   string
			dirty    bool
			err      error
			nilError bool
		)

		version, commit, dirty, err = semanticVersion([]byte(datum.description))
		nilError = (err == nil)
		if strings.Compare(datum.version, version) != 0 ||
			strings.Compare(datum.commit, commit) != 0 ||
			datum.dirty != dirty ||
			datum.nilError != nilError {
			t.Errorf(
				`
executed: %s(%q)
expected: %q, %q, %t, %s
received: %q, %q, %t, %s
`,
				functionName, datum.description,
				datum.version, datum.commit, datum.dirty, showError[datum.nilError],
				version, commit, dirty, showError[nilError],
			)
		}
	}
}
